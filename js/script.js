/*jslint browser: true*/
/*global $, jQuery, alert*/
$(function () {
    'use strict';
    
    // Banner
    $('.banner').bxSlider({
        controls: false,
        auto: true,
        pause: 5000,
        pagerCustom: '#banner_pager',
        onSliderLoad: function () {
            $('.banner>li .cap_block').eq(1).addClass('active-slide');
            $(".cap_block.active-slide").addClass("wow animated fadeInUp");
        },
        onSlideAfter: function (currentSlideNumber, totalSlideQty, currentSlideHtmlObject) {
            //console.log(currentSlideHtmlObject);
            $('.active-slide').removeClass('active-slide');
            $('.banner>li .cap_block').eq(currentSlideHtmlObject + 1).addClass('active-slide');
            $(".cap_block.active-slide").addClass("wow animated fadeInUp");

        },
        onSlideBefore: function () {
            $(".cap_block.active-slide").removeClass("wow animated fadeInUp");
            $(".one.cap_block.active-slide").removeAttr('style');
        }
    });
    
    // Testimonial
    $('.testimonial').bxSlider({
        pager: false,
        auto: true
    });
    
    // Navigation drop down
    $('.nav_trigger').click(function(){
        $('.res_nav').slideToggle();
    });
    $(window).resize(function(){
        var windWidth = $(window).width();
        if(windWidth >= 768){
            $('.res_nav').hide();
        }
    });
    
    // Adding active class to navigation menu
    var page_id = $('.page_identifier').data('page_id');
    //console.log(page_id);
    $('ul.navigation > li > a, ul.res_nav > li > a').each(function(){
    	var curPage = $(this);
    	var pageName = $.trim(curPage.text());
    	if(pageName == page_id){
			curPage.addClass('active');
			curPage.attr('href', '#');
		}
    });
    // Gallery Slider
    $('.gal_slider').bxSlider({
        auto: true,
        prevSelector: '#gal_prev',
        nextSelector: '#gal_next',
        prevText: '<i class="fa fa-angle-left"></i>',
        nextText: '<i class="fa fa-angle-right"></i>'
    });
    // Date picker
    $('#datepicker').datepicker();
    
    // Contact form
    /*$('#contact').click(function(e){
        e.preventDefault();
        var data = $('.contact_form').serializeArray();
        //console.log(data);
        $.post('mail.php', data, function(data){
            //console.log(data.status);
            if(data.status){
                alert('Thank you for your feedback.');
            } else {
                alert('Oops! Something went wrong. Please try again later.');
            }
        });
    });*/
    // Book Form
    /*$('#book').click(function(e){
        e.preventDefault();
        var data = $('.book_form').serializeArray();
        //console.log(data);
        $.post('book.php', data, function(data){
            //console.log(data.status);
            if(data.status){
                alert('We got your detail. We will call you soon.');
            } else {
                alert('Oops! Something went wrong. Please try again later.');
            }
        });
    });*/
    
    
    $('.contact_form').submit(function(e){
        e.preventDefault();
        checkVal();
    });
    function checkVal(){
        var proceed = false;
        if( $(".contact_form")[0].checkValidity ){
            if ($(".contact_form")[0].checkValidity()) {
                proceed = true;
            }
        }else{
            proceed = true;
        }
        if (proceed) {
            //Do ajax call
            var data = $('.contact_form').serializeArray();
            //console.log(data);
            $.post('mail.php', data, function(data){
                console.log(data.status);                
                /*(data.status){
                    alert('Thank you for your feedback.');
                } else {
                    alert('Oops! Something went wrong. Please try again later.');
                }*/
            });
        }
    }
    $('.book_form').submit(function(e){
        e.preventDefault();
        bookVal();
    });
    function bookVal(){
        var proceed = false;
        if( $(".book_form")[0].checkValidity ){
            if ($(".book_form")[0].checkValidity()) {
                proceed = true;
            }
        }else{
            proceed = true;
        }
        if (proceed) {
            //Do ajax call
            var data = $('.book_form').serializeArray();
            //console.log(data);
            $.post('book.php', data, function(data){
                //console.log(data.status);
                if(data.status){
                    alert('We got your details. We will call you soon.');
                } else {
                    alert('Oops! Something went wrong. Please try again later.');
                }
            });
        }
    }
});






















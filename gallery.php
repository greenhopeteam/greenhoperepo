<?php require('header.php'); ?>
<div class="gallery_wrap page_identifier" data-page_id="gallery">
    <h3 class="gallery_head">latest from the showcase</h3>
    <div class="gal_slide_wrap">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="gal_inner">
                        <div class="gal_controls">
                            <span id="gal_prev"></span>
                            <span id="gal_next"></span>
                        </div>
                        <ul class="gal_slider">
                            <li><img src="images/gal01.jpg" alt="" /></li>
                            <li><img src="images/gal02.jpg" alt="" /></li>
                            <li><img src="images/gal03.jpg" alt="" /></li>
                            <li><img src="images/gal04.jpg" alt="" /></li>
                            <li><img src="images/gal05.jpg" alt="" /></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>   
</div>
<div class="gal_pop">
    <div class="container">
        <div id="pop_show">
            <a class="pop_up" href="images/gal01.jpg">
                <div class="pop_up_inner">
                    <div class="pop">
                        <img src="images/gal01.jpg" alt="" />
                        <div class="pop_overlay"></div>
                    </div>                        
                </div>
            </a>
            <a class="pop_up" href="images/gal02.jpg">
                <div class="pop_up_inner">
                    <div class="pop">
                        <img src="images/gal02.jpg" alt="" />
                        <div class="pop_overlay"></div>
                    </div>                        
                </div>
            </a>
            <a class="pop_up" href="images/gal03.jpg">
                <div class="pop_up_inner">
                    <div class="pop">
                        <img src="images/gal03.jpg" alt="" />
                        <div class="pop_overlay"></div>
                    </div>                        
                </div>
            </a>
            <a class="pop_up" href="images/gal04.jpg">
                <div class="pop_up_inner">
                    <div class="pop">
                        <img src="images/gal04.jpg" alt="" />
                        <div class="pop_overlay"></div>
                    </div>                        
                </div>
            </a>
            <div class="bd_clear"></div>
        </div>
    </div>
</div>
<?php require('footer.php'); ?>
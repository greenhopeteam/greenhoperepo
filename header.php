<!DOCTYPE HTML>
<html lang="">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Green Hoppers</title>
    <link rel="icon" href="images/fav_ico.png">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css">
    <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <link rel="stylesheet" type="text/css" href="light/lightgallery.css">
    <link rel="stylesheet" type="text/css" href="css/jquery-ui-1.9.2.custom.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
    
    
    
    <script src="js/jquery-2.1.4.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.bxslider.js"></script>
    <script src="js/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="js/script.js"></script>
    
    
</head>
    
<body>
    <!-- Header -->
    <div class="header_wrap">
        <div class="container header">
            <a class="logo" href="index.html">
                <img src="images/logo.png" alt="Green Hoppers" />
            </a>
            <div class="nav_wrap">
                <ul class="navigation">
                    <li><a href="index.php">home</a></li>
                    <li><a href="aboutus.php">about</a></li>
                    <!--<li><a href="#">our mission</a></li>-->
                    <li><a href="gallery.php">gallery</a></li>
                    <li><a href="services.php">services</a></li>
                    <li><a href="contactus.php">contact</a></li>
                </ul>
                <a class="book_lnk" data-toggle="modal" data-target="#myModal" href="#"><img src="images/mouse.png" alt="" />book online</a>
                <div class="bd_clear"></div>
            </div>
            <div class="bd_clear"></div>
            <span class="nav_trigger"><i class="fa fa-bars"></i></span>
        </div>    
        <ul class="res_nav">
            <li><a href="index.php">home</a></li>
            <li><a href="aboutus.php">about</a></li>
            <!--<li><a href="#">our mission</a></li>-->
            <li><a href="gallery.php">gallery</a></li>
            <li><a href="services.php">services</a></li>
            <li><a href="contact.php">contact</a></li>
            <li><a class="book" data-toggle="modal" data-target="#myModal" href="#">book online</a></li>
        </ul>
    </div>
    <!-- /Header -->
<?php require('header.php'); ?>
<div class="contact_wrap">
    <div class="contact_inner">
        <form method="post" action="mail.php" class="contact_form">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-8">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="contact_head">
                                    <h3>contact us</h3>
                                    <p>Feel free to contact us</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="form_block">
                                    <input type="text" name="name" placeholder="Name" required />
                                    <span><i class="fa fa-user"></i></span>
                                </div>
                                <div class="form_block">
                                    <input type="text" name="mobile" placeholder="Phone" />
                                    <span><i class="fa fa-phone"></i></span>
                                </div>
                                <div class="form_block">
                                    <input type="email" name="email" placeholder="Email" required />
                                    <span><i class="fa fa-at"></i></span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="form_block">
                                    <textarea name="message" placeholder="Message" required></textarea>
                                    <span><i class="fa fa-edit"></i></span>
                                    <input id="contact" type="submit" name="submit" value="Submit" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="address">
                            <div class="address_inner">
                                <div class="address_ico">
                                    <i class="fa fa-home"></i>
                                </div>
                                <p>35 West Streat, 15th Street,<br />Calicut, Kerala</p>
                                <div class="contact_block">                                
                                    <span><i class="fa fa-phone"></i></span><a href="tel:0495002255">0495 222 555</a> 
                                </div>
                                <div class="contact_block">                                
                                    <span><i class="fa fa-envelope"></i></span><a href="mailto:info@greenhoppers.com">info@greenhoppers.com</a>                                   </div>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<?php require('footer.php'); ?>
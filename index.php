<?php require('header.php'); ?>
<!-- Banner -->
    <div class="banner_wrap page_identifier" data-page_id="home">
        <ul class="banner">
            <li>
                <div class="banner_box banner_box1">
                    <div class="container cap_wrap">
                        <div class="cap_block">
                            <h3>Lorem ipsum dolor sit amet</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic corrupti, id! Distinctio excepturi quidem magnam ad mollitia perspiciatis repellat doloribus, rerum consequatur, tenetur facilis deleniti et. Itaque quia, dolore tempore.</p>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="banner_box banner_box2">
                    <div class="container cap_wrap">
                        <div class="cap_block">
                            <h3>Lorem ipsum dolor sit amet</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic corrupti, id! Distinctio excepturi quidem magnam ad mollitia perspiciatis repellat doloribus, rerum consequatur, tenetur facilis deleniti et. Itaque quia, dolore tempore.</p>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="banner_box banner_box3">
                    <div class="container cap_wrap">
                        <div class="cap_block">
                            <h3>Lorem ipsum dolor sit amet</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic corrupti, id! Distinctio excepturi quidem magnam ad mollitia perspiciatis repellat doloribus, rerum consequatur, tenetur facilis deleniti et. Itaque quia, dolore tempore.</p>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
        <div id="banner_pager" class="banner_pager">
            <a data-slide-index="0" href=""><span></span></a>
            <a data-slide-index="1" href=""><span></span></a>
            <a data-slide-index="2" href=""><span></span></a>
        </div>
    </div>
    <!-- /Banner -->
    
    <!-- Highlight Section -->
    <div class="container heighlight">
        <div class="row">
            <div class="col-lg-12">
                <div class="high_hd">                      
                    <h3>highlights &#64;</h3>
                    <img src="images/high_logo.png" alt="" />
                </div>
                <div class="high_hd_ul">
                    <span class="high_hd_ul_inner"><span class="high_ul"></span></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="high_list_bg">
                    <div class="row">
                        <div class="col-lg-12">
                            <h3 class="high_capt high_capt_top">ayurveda<br />center</h3>
                        </div>
                    </div>
                    <div class="row high_cap_rowb">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <h3 class="high_capt high_capt_bleft"><span>dinning<br />room</span></h3>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <h3 class="high_capt high_capt_bright"><span>swimming<br />pool</span></h3>
                        </div>
                    </div>                    
                    <div class="row high_cap_rowc">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <h3 class="high_capt high_capt_cleft"><span>Multicuisine<br />restaurant</span></h3>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <h3 class="high_capt high_capt_cright"><span>laundry<br />services</span></h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container res_high_light">
        <div class="res_high_block">
            <img src="images/high1.png" alt="" />
            <h3>ayurveda<br />center</h3>
        </div>
        <div class="res_high_block">
            <img src="images/high2.png" alt="" />
            <h3>dining<br />room</h3>
        </div>
        <div class="res_high_block">
            <img src="images/high3.png" alt="" />
            <h3>swimming<br />pool</h3>
        </div>
        <div class="res_high_block">
            <img src="images/high4.png" alt="" />
            <h3>Multicuisine<br />restaurant</h3>
        </div>
        <div class="res_high_block">
            <img src="images/high5.png" alt="" />
            <h3>laundry<br />services</h3>
        </div>
    </div>
    
    <!-- Welcome Area -->
    <div class="welcome_wrap">
        <div class="container">
            <div class="col-lg-6 col-md-6 col-sm-6 welcome_res_img">
                <img src="images/wel_res_img.jpg" alt="" />                
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 welcome_text">
                <h3>Welcome to</h3>
                <div class="welcome_img">
                    <img src="images/welcome_logo.png" alt="" />
                </div>                
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sit amet volutpat enim. In eleifend blandit orci, eu porttitor lectus feugiat vitae. Maecenas egestas vestibulum nunc vitae pulvinar. Vestibulum posuere egestas tempor. Cras interdum accumsan elit, nec dignissim massa consectetur et. Integer tempor ullamcorper risus eu venenatis. Integer facilisis quam et consequat hendrerit. In malesuada nulla at ligula congue, a ullamcorper velit viverra.</p>
                <div class="welcome_lnk">
                    <span></span>
                    <a href="#">read more</a>
                </div>
            </div>
        </div>
    </div>
    <!-- /Welcome -->
    
    <!-- Testimonial -->
    <div class="container">
        <div class="testimonial_wrap">
            <h3 class="testimonial_head">clients feedback</h3>
            <div class="testimonial_head_brdr">
                <span></span>
            </div>
            <div class="teti_quote_ico">
                <i class="fa fa-quote-left"></i>
            </div>
            <div class="testimonial_outer">
                <ul class="testimonial">
                    <li>
                        <p class="testimonial_text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book Lorem Ipsum is simply </p>
                        <h3>Richard Bill</h3>
                        <p class="designation">CEO, Lorem Ipsum</p>
                    </li>
                    <li>
                        <p class="testimonial_text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book Lorem Ipsum is simply </p>
                        <h3>Richard Bill</h3>
                        <p class="designation">CEO, Lorem Ipsum</p>
                    </li>
                    <li>
                        <p class="testimonial_text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book Lorem Ipsum is simply </p>
                        <h3>Richard Bill</h3>
                        <p class="designation">CEO, Lorem Ipsum</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- /Testimonial -->
<?php require('footer.php'); ?>
<?php require('header.php'); ?>
<div class="service_wrap">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 serv_head">
                <h2>Services</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic corrupti, id! Distinctio excepturi quidem magnam ad mollitia perspiciatis repellat doloribus.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="serv_col">
                    <img src="images/high1.png" alt="" />
                    <h3>Ayurveda Center</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic corrupti, id! Distinctio excepturi quidem magnam ad mollitia perspiciatis repellat doloribus.</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="serv_col">
                    <img src="images/high2.png" alt="" />
                    <h3>Dinning Room</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic corrupti, id! Distinctio excepturi quidem magnam ad mollitia perspiciatis repellat doloribus.</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="serv_col">
                    <img src="images/high3.png" alt="" />
                    <h3>Swimming Pool</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic corrupti, id! Distinctio excepturi quidem magnam ad mollitia perspiciatis repellat doloribus.</p>
                </div>
            </div>
        </div>
        <div class="row">            
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="serv_col">
                    <img src="images/high4.png" alt="" />
                    <h3>Multicuisine restaurant</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic corrupti, id! Distinctio excepturi quidem magnam ad mollitia perspiciatis repellat doloribus.</p>
                </div>
            </div>        
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="serv_col">
                    <img src="images/high5.png" alt="" />
                    <h3>laundry services</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic corrupti, id! Distinctio excepturi quidem magnam ad mollitia perspiciatis repellat doloribus.</p>
                </div>
            </div>   
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="serv_col">
                    <img src="images/high1.png" alt="" />
                    <h3>Lorem Ipsum</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic corrupti, id! Distinctio excepturi quidem magnam ad mollitia perspiciatis repellat doloribus.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require('footer.php'); ?>
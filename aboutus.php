<?php require('header.php'); ?>
<div class="about_feat_wrap page_identifier" data-page_id="about">
    <div class="about_feat">
        <div class="feat_block">
            <h3>about us</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sodales, libero id pretium cursus, lectus massa maximus ante, non porta nisi odio sed nunc. Sed in convallis erat. Sed et dolor ut risus faucibus bibendum eget vel lectus. Nunc laoreet cursus lacinia. Vivamus euismod dolor id scelerisque vestibulum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
        </div>
    </div>
</div>
<div class="vision_wrap">
    <div class="container">
        <h3 class="vision_head">our vision</h3>
        <div class="vision_head_brdr">
            <span></span>
        </div>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sodales, libero id pretium cursus, lectus massa maximus ante, non porta nisi odio sed nunc. Sed in convallis erat. Sed et dolor ut risus faucibus bibendum eget vel lectus. Nunc laoreet cursus lacinia. Vivamus euismod dolor id scelerisque vestibulum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sodales, libero id pretium cursus, lectus massa maximus ante, non porta nisi odio sed nunc. Sed in convallis erat. Sed et dolor ut risus faucibus bibendum eget vel lectus. Nunc laoreet cursus lacinia. Vivamus euismod dolor id scelerisque vestibulum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.<br /><br />Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sodales, libero id pretium cursus, lectus massa maximus ante, non porta nisi odio sed nunc. Sed in convallis erat. Sed et dolor ut risus faucibus bibendum eget vel lectus. Nunc laoreet cursus lacinia. Vivamus euismod dolor id scelerisque vestibulum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
    </div>
</div>
<?php require('footer.php'); ?>
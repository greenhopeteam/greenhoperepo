<!-- Footer -->
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="footer_soc">
                        <a href="#"><i class="fa fa-instagram"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-google-plus"></i></a>
                        <a href="#"><i class="fa fa-facebook"></i></a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="footer_logo">
                        <a href="index.html"><img src="images/footer_logo.png" alt="" /></a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="footer_book">
                        <a href="#" data-toggle="modal" data-target="#myModal"><img src="images/mouse.png" alt="">book online</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="copy_wrap">
                        <div class="copy">Copyright &copy; 2015 Green Hoppers</div>
                        <div class="power">Powered by <a href="http://www.bodhiinfo.com" target="_blank"><img src="images/bodhi_logo2.png" alt="" /></a></div>
                        <div class="bd_clear"></div>
                    </div>                    
                </div>
            </div>
        </div>
    </div>

    <div id="myModal" class="modal fade book_modal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Book Online</h4>
                </div>
                <div class="modal-body">
                    <form class="book_form" method="post" action="book.php">
                        <div class="book_block">
                            <input type="text" class="form-control" required name="name" placeholder="Name" />
                        </div>
                        <div class="book_block">
                            <input type="text" class="form-control" required name="mobile" placeholder="Phone/Mobile" />
                        </div>
                        <div class="book_block">
                            <input type="email" class="form-control" required name="email" placeholder="Email" />
                        </div>
                        <div class="book_block">
                            <input type="text" class="form-control" required name="place" placeholder="Place" />
                        </div>
                        <div class="book_block">
                            <input type="text" class="form-control" name="arrival" id="datepicker" required placeholder="Date of arrival" />
                        </div>
                        <div class="book_block">
                            <textarea name="requirements" class="form-control" placeholder="Your requirements" required></textarea>
                        </div>
                        <div class="book_block">
                            <button type="submit" name="submit" id="book" class="btn btn-success">Book Now</button>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script src="light/lightgallery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>
    <script src="light/lg-fullscreen.min.js"></script>
    <script src="light/lg-thumbnail.min.js"></script>
    <script>
        $(function(){
            $("#pop_show").lightGallery({
                download: false,
                thumbnail: false,
                fullScreen: false
            });      
        });
    </script>

</body>
</html>